{{ range $port_map := .PROXY_PORT_MAP | split " " }}
{{ $port_map_list := $port_map | split ":" }}
{{ $scheme := index $port_map_list 0 }}
{{ $listen_port := index $port_map_list 1 }}
{{ $upstream_port := index $port_map_list 2 }}

{{ if eq $scheme "https"}}
server {
  listen      [{{ $.NGINX_BIND_ADDRESS_IP6 }}]:443 ssl http2;
  listen      {{ if $.NGINX_BIND_ADDRESS_IP4 }}{{ $.NGINX_BIND_ADDRESS_IP4 }}:{{end}}443 ssl http2;
  server_name {{ $.APP }}.app.softgen.ai;

  ssl_certificate           {{ $.APP_SSL_PATH }}/server.crt;
  ssl_certificate_key       {{ $.APP_SSL_PATH }}/server.key;
  ssl_protocols             TLSv1.2 {{ if eq $.TLS13_SUPPORTED "true" }}TLSv1.3{{ end }};
  ssl_prefer_server_ciphers off;

  keepalive_timeout   70;

  location / {
    proxy_pass  http://{{ $.APP }}-3080;
    include     {{ $.DOKKU_ROOT }}/{{ $.APP }}/nginx.conf.d/*.conf;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Connection $http_connection;
    proxy_set_header Host $http_host;
  }

  location /_next/webpack-hmr {
    proxy_pass http://{{ $.APP }}-3080/_next/webpack-hmr;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }

  # Error pages and additional settings...
}

server {
  listen      [{{ $.NGINX_BIND_ADDRESS_IP6 }}]:443 ssl http2;
  listen      {{ if $.NGINX_BIND_ADDRESS_IP4 }}{{ $.NGINX_BIND_ADDRESS_IP4 }}:{{end}}443 ssl http2;
  server_name {{ $.APP }}-api.app.softgen.ai;

  ssl_certificate           {{ $.APP_SSL_PATH }}/server.crt;
  ssl_certificate_key       {{ $.APP_SSL_PATH }}/server.key;
  ssl_protocols             TLSv1.2 {{ if eq $.TLS13_SUPPORTED "true" }}TLSv1.3{{ end }};
  ssl_prefer_server_ciphers off;

  keepalive_timeout   70;

  location / {
    proxy_pass  http://{{ $.APP }}-8080;
    include     {{ $.DOKKU_ROOT }}/{{ $.APP }}/nginx.conf.d/*.conf;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
    proxy_set_header Host $http_host;
  }

  # Error pages and additional settings...
}
{{ end }}
{{ end }}

{{ if $.DOKKU_APP_WEB_LISTENERS }}
upstream {{ $.APP }}-3080 {
{{ range $listeners := $.DOKKU_APP_WEB_LISTENERS | split " " }}
{{ $listener_list := $listeners | split ":" }}
{{ $listener_ip := index $listener_list 0 }}
  server {{ $listener_ip }}:3080;{{ end }}
}

upstream {{ $.APP }}-8080 {
{{ range $listeners := $.DOKKU_APP_WEB_LISTENERS | split " " }}
{{ $listener_list := $listeners | split ":" }}
{{ $listener_ip := index $listener_list 0 }}
  server {{ $listener_ip }}:8080;{{ end }}
}
{{ end }}