import { Box, Text, VStack } from '@chakra-ui/react';

export const LandingPage = () => {
  return (
    <Box>
      <VStack>
        <Text>Welcome to the Landing Page</Text>
      </VStack>
    </Box>
  );
};