import { Text, HStack } from '@chakra-ui/react';
import { AiFillStar } from 'react-icons/ai';

export const BrandLogo = () => (
  <HStack spacing={2} alignItems="center">
    <AiFillStar size="32px" />
    <Text fontSize="xl" fontWeight="bold">
      Marko Kraemer
    </Text>
  </HStack>
);