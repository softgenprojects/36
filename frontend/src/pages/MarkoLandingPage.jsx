import { Box, VStack, Image, Heading, Text, Link, useBreakpointValue } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';

const MarkoLandingPage = () => {
  const bgGradient = useBreakpointValue({
    base: 'linear(to-t, gray.300, white)',
    md: 'linear(to-l, blackAlpha.700, whiteAlpha.700)'
  });

  return (
    <Box
      minH='100vh'
      bgGradient={bgGradient}
      display='flex'
      alignItems='center'
      justifyContent='center'
    >
      <VStack
        spacing={8}
        p={5}
        alignItems='center'
        maxW='lg'
        m='auto'
      >
        <BrandLogo />
        <Image
          borderRadius='full'
          boxSize='150px'
          src='https://media.licdn.com/dms/image/D4E03AQEEIDe7V9k_9w/profile-displayphoto-shrink_800_800/0/1687302199230?e=1711584000&v=beta&t=EuY1SQpu8yQ9xN39KyySnHoApPjwWTrTxEQNeR5qnlk'
          alt='Marko Kraemer'
        />
        <Heading as='h1' size='xl'>
          Marko Kraemer
        </Heading>
        <Text fontSize='md' textAlign='center'>
          CEO & Co-Founder at SoftGen.ai
        </Text>
        <Text fontSize='sm' textAlign='center' px={2}>
          "Hey, my name is Marko! I am 18 years old and I have been running software projects and companies since I was 13. I have scaled up a software development agency to 1+ million euros in revenue, and over the last few years, most of my time has been spent planning and managing custom software development projects. When I first used Generative AI to plan out the complete business logic and data schema in 1.5 hours instead of 8 when creating tasks for my developers, I knew a big change was coming to this industry. This led to the creation of SoftGen.AI, a comprehensive development solution that generates the complete codebase based on project requirements."
        </Text>
        <Text fontSize='sm' textAlign='center' px={2}>
          "The rise of GPT and AI is changing the economy - in 10 years it will be fundamentally different than it is today. Latest GPT models allow autonomous AI's to be deployed. Existing processes need to be rethought based on first-principle thinking in all industries; we will be part of that change for custom software development. Whether SoftGen.AI exists or not, autonomous developer AI's are coming; the technology is here, and the execution is important now."
        </Text>
        <VStack divider={<Box borderColor='gray.200' />} spacing={4} align='stretch'>
          <Link href='#' isExternal>
            SoftGen.AI
          </Link>
          <Link href='#' isExternal>
            Projects
          </Link>
          <Link href='#' isExternal>
            Contact
          </Link>
        </VStack>
      </VStack>
    </Box>
  );
};

export default MarkoLandingPage;
